Continuous Integration automated site build for Drupal
======================================================

Intoduction
-----------
TODO

Installation
---------------
 . Setup the cibuild container (see github)
 . create /home/builder and checkout this repo into /home/builder/drupal-ci
 . Optional: Add sample DB, files, tests to /home/builder/source-....
 . Optional: Edit playbooks/settings.yml and create playbooks/build/PROJECT-test-site-build.yml
 . test a build on the command line, then test from Jenkiins (see below)


Running Drupal profile install
------------------------------
New build on the command line (e.g. branch=develop build=2 for starterkit, standard profile)

su - jenkins
sudo /home/builder/drupal-ci/site-build.sh develop 2 starterkit https://git.vptt.ch/git/webfactory-user-website.git standard

# starterkit profile
sudo /home/builder/drupal-ci/site-build.sh develop 2 starterkit https://git.vptt.ch/git/webfactory-user-website.git starterkit

# +expect included tests and do a feature revert
sudo /home/builder/drupal-ci/site-build.sh develop 2 starterkit https://git.vptt.ch/git/webfactory-user-website.git starterkit n y

# Call build from a jenkins job
ssh -p 2201 jenkins@cibuild.webfact.vptt.ch "SUDO_LINE_AS_ABOVE"



Runing with Demo data
-----------------------
Sample data
  source-dbs/PROJECT/xx.mysql.gz
  source-files/PROJECT/xx.tar.gz
  source-other: tests, if the option to include default tests is enabled. 

# +default tests and do a feature revert
sudo /home/builder/drupal-ci/site-build.sh develop 2 starterkit https://git.vptt.ch/git/webfactory-user-website.git starterkit y y

## Thanks ##
To webscope.co.nz who provided the original code, that was then rewritten to a Container and non pantheon environment.

